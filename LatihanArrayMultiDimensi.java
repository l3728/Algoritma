package latihan.algoritma;
public class LatihanArrayMultiDimensi {
    public static void main(String[] args) {
        String mahasiswa [][]={
        {
            "021210025", "Liana"
        },
        {
            "021210088", "Bagus"
        },
        {
            "021210062", "Ferli"
        },
        {
            "021210045", "Vinkan"
        },
        {
            "021210099", "Hani"
        }
    };
    System.out.println("Nama "+ mahasiswa[2][1]+" " +"Berada pada baris 3, kolom 2");
    System.out.println(mahasiswa[2][1]+" "+"Berasa pada baris 3, kolom 2");
    System.out.println("NPM "+ mahasiswa[2][0]+" "+"Berada pada baris 3, kolom 1");
    
    }
    
}
